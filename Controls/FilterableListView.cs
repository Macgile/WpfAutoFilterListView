﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Linq;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Input;

// ReSharper disable ConvertToAutoProperty
// ReSharper disable ConvertToAutoPropertyWhenPossible
// ReSharper disable ArrangeAccessorOwnerBody
// ReSharper disable InvertIf
// ReSharper disable NonReadonlyMemberInGetHashCode

namespace WpfBindingFiltering.Controls
{
    /// <inheritdoc />
    /// <summary>
    /// Extends ListView to provide filterable columns
    /// </summary>
    public class FilterableListView : SortableListView
    {
        #region dependency properties

        public static readonly DependencyProperty FilterButtonActiveStyleProperty =
            DependencyProperty.Register("FilterButtonActiveStyle", typeof(Style), typeof(FilterableListView),
                new UIPropertyMetadata(null));

        public static readonly DependencyProperty FilterButtonInactiveStyleProperty =
            DependencyProperty.Register("FilterButtonInActiveStyle", typeof(Style), typeof(FilterableListView),
                new UIPropertyMetadata(null));

        /// <summary>
        /// The style applied to the filter button when it is an active state
        /// </summary>
        public Style FilterButtonActiveStyle
        {
            get { return (Style) GetValue(FilterButtonActiveStyleProperty); }
            set { SetValue(FilterButtonActiveStyleProperty, value); }
        }

        /// <summary>
        /// The style applied to the filter button when it is an inactive state
        /// </summary>
        public Style FilterButtonInactiveStyle
        {
            get { return (Style) GetValue(FilterButtonInactiveStyleProperty); }
            set { SetValue(FilterButtonInactiveStyleProperty, value); }
        }

        #endregion dependency properties

        #region Public Fields

        public static readonly ICommand ShowFilter = new RoutedCommand();

        #endregion Public Fields

        #region Private Fields

        private readonly Hashtable currentFilters = new Hashtable();
        private ArrayList filterList;

        #endregion Private Fields

        #region inner classes

        /// <summary>
        /// A simple data holder for passing information regarding filter clicks
        /// </summary>
        private struct FilterStruct
        {
            #region Public Fields

            public Button button;
            public string property;
            public FilterItem value;

            #endregion Public Fields

            #region Public Constructors

            public FilterStruct(string property, Button button, FilterItem value)
            {
                Debug.WriteLine("FilterStruct Constructors");

                this.value = value;
                this.button = button;
                this.property = property;
            }

            #endregion Public Constructors
        }

        /// <inheritdoc />
        /// <summary>
        /// The items which are bound to the drop down filter list
        /// </summary>
        private class FilterItem : IComparable
        {
            /// <summary>
            /// The filter item instance
            /// </summary>
            private object item;

            public object Item
            {
                get { return item; }
                set { item = value; }
            }

            /// <summary>
            /// The item viewed in the filter drop down list. Typically this is the same as the item
            /// property, however if item is null, this has the value of "[empty]"
            /// </summary>
            private object itemView;

            public object ItemView
            {
                get { return itemView; }
                set { itemView = value; }
            }

            public FilterItem(IComparable item)
            {
                Debug.WriteLine("FilterItem Constructors");

                this.item = itemView = item;
                if (item == null)
                {
                    itemView = "[empty]";
                }
            }

            public override int GetHashCode()
            {
                return item != null ? item.GetHashCode() : 0;
            }

            public override bool Equals(object obj)
            {
                FilterItem otherItem = obj as FilterItem;

                Debug.WriteLine($"\t\tFilterItem Equals {otherItem?.Item}");

                if (otherItem != null)
                {
                    if (otherItem.Item == this.Item)
                    {
                        return true;
                    }
                }

                return false;
            }

            public int CompareTo(object obj)
            {
                Debug.WriteLine("\t\tFilterItem CompareTo");

                FilterItem otherFilterItem = (FilterItem) obj;

                if (this.Item == null && obj == null)
                {
                    return 0;
                }
                else if (otherFilterItem.Item != null && this.Item != null)
                {
                    return ((IComparable) item).CompareTo((IComparable) otherFilterItem.item);
                }
                else
                {
                    return -1;
                }
            }
        }

        #endregion inner classes

        #region Public Constructors

        public FilterableListView()
        {
            CommandBindings.Add(new CommandBinding(ShowFilter, ShowFilterCommand));
        }

        #endregion Public Constructors

        #region Public Methods

        public override void OnApplyTemplate()
        {
            Debug.WriteLine("OnApplyTemplate");

            base.OnApplyTemplate();

            // ensure that the custom inactive style is applied
            if (FilterButtonInactiveStyle != null)
            {
                var columnHeaders = Helpers.FindElementsOfType(this, typeof(GridViewColumnHeader));

                foreach (var columnHeader in columnHeaders)
                {
                    var button = (Button) Helpers.FindElementOfType(columnHeader, typeof(Button));
                    if (button != null)
                    {
                        button.Style = FilterButtonInactiveStyle;
                    }
                }
            }
        }

        #endregion Public Methods

        #region Protected Methods

        protected override void OnInitialized(EventArgs e)
        {
            Debug.WriteLine("OnInitialized");


            base.OnInitialized(e);

            var uri = new Uri("/Controls/FiterListViewDictionary.xaml", UriKind.Relative);
            Dictionary = Application.LoadComponent(uri) as ResourceDictionary;

            // cast the ListView's View to a GridView
            var gridView = (GridView) View;
            if (gridView != null)
            {
                // apply the data template, that includes the popup, button etc ... to each column
                foreach (var gridViewColumn in gridView.Columns)
                {
                    if (Dictionary != null)
                        gridViewColumn.HeaderTemplate = (DataTemplate) Dictionary["FilterGridHeaderTemplate"];
                }
            }
        }

        #endregion Protected Methods

        #region Private Methods

        private void AddFilter(string property, FilterItem value, Button button)
        {
            Debug.WriteLine("AddFilter");

            if (currentFilters.ContainsKey(property))
            {
                currentFilters.Remove(property);
            }

            currentFilters.Add(property, new FilterStruct(property, button, value));
        }

        /// <summary>
        /// Applies the current filter to the list which is being viewed
        /// </summary>
        private void ApplyCurrentFilters()
        {
            Debug.WriteLine("ApplyCurrentFilters");

            if (currentFilters.Count == 0)
            {
                Items.Filter = null;
                return;
            }

            // construct a filter and apply it
            Items.Filter = delegate(object item)
            {
                // when applying the filter to each item, iterate over all of
                // the current filters
                var match = true;

                // var values = currentFilters.Values;

                foreach (FilterStruct filter in currentFilters.Values)
                {
                    // obtain the value for this property on the item under test
                    var filterPropDesc = TypeDescriptor.GetProperties(typeof(Employee))[filter.property];
                    var itemValue = filterPropDesc.GetValue((Employee) item);

                    if (itemValue != null)
                    {
                        // check to see if it meets our filter criteria
                        if (!itemValue.Equals(filter.value.Item))
                            match = false;
                    }
                    else
                    {
                        if (filter.value.Item != null)
                            match = false;
                    }
                }

                return match;
            };
        }

        private bool IsPropertyFiltered(string property)
        {
            Debug.WriteLine("IsPropertyFiltered");

            foreach (string filterProperty in currentFilters.Keys)
            {
                var filter = (FilterStruct) currentFilters[filterProperty];
                if (filter.property == property)
                    return true;
            }

            return false;
        }

        /// <summary>
        /// Handles the selection change event from the filter popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SelectionChangedHandler(object sender, SelectionChangedEventArgs e)
        {
            Debug.WriteLine("SelectionChangedHandler");

            // obtain the term to filter for
            var filterListView = (ListView) sender;
            var filterItem = (FilterItem) filterListView.SelectedItem;

            // navigate up to the header to obtain the filter property name
            var header =
                (GridViewColumnHeader) Helpers.FindElementOfTypeUp(filterListView, typeof(GridViewColumnHeader));

            var column = (SortableGridViewColumn) header.Column;
            var currentFilterProperty = column.SortPropertyName;

            if (filterItem == null)
                return;

            // determine whether to clear the filter for this column
            if (filterItem.ItemView.Equals("[clear]"))
            {
                if (currentFilters.ContainsKey(currentFilterProperty))
                {
                    var filter = (FilterStruct) currentFilters[currentFilterProperty];
                    filter.button.ContentTemplate = (DataTemplate) Dictionary["FilterButtonInactiveTemplate"];
                    if (FilterButtonInactiveStyle != null)
                    {
                        filter.button.Style = FilterButtonInactiveStyle;
                    }

                    currentFilters.Remove(currentFilterProperty);
                }

                ApplyCurrentFilters();
            }
            else
            {
                // find the button and apply the active style
                var button = (Button) Helpers.FindVisualElement(header, "filterButton");
                button.ContentTemplate = (DataTemplate) Dictionary["FilterButtonActiveTemplate"];

                if (FilterButtonActiveStyle != null)
                {
                    button.Style = FilterButtonActiveStyle;
                }

                AddFilter(currentFilterProperty, filterItem, button);
                ApplyCurrentFilters();
            }

            // navigate up to the popup and close it
            var popup = (Popup) Helpers.FindElementOfTypeUp(filterListView, typeof(Popup));
            popup.IsOpen = false;
        }

        /// <summary>
        /// Handles the ShowFilter command to populate the filter list and display the popup
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ShowFilterCommand(object sender, ExecutedRoutedEventArgs e)
        {
            Debug.WriteLine("ShowFilterCommand");

            var button = (Button) e.OriginalSource;

            if (button != null)
            {
                // navigate up to the header
                var header =
                    (GridViewColumnHeader) Helpers.FindElementOfTypeUp(button, typeof(GridViewColumnHeader));

                // then down to the popup
                var popup = (Popup) Helpers.FindElementOfType(header, typeof(Popup));

                if (popup != null)
                {
                    // find the property name that we are filtering
                    var column = (SortableGridViewColumn) header.Column;
                    var propertyName = column.SortPropertyName;

                    // clear the previous filter
                    if (filterList == null)
                    {
                        filterList = new ArrayList();
                    }

                    filterList.Clear();

                    // if this property is currently being filtered, provide an option to clear the filter.
                    if (IsPropertyFiltered(propertyName))
                    {
                        filterList.Add(new FilterItem("[clear]"));
                    }
                    else
                    {
                        var type = GetElementTypeOfEnumerable2(Items);

                        var filterPropDesc = TypeDescriptor.GetProperties(type)[propertyName];

                        var list = Items.Cast<object>()
                            .Select(item => filterPropDesc.GetValue(item))
                            .Distinct().OrderBy(o => o).ToList();

                        if (list.Count > 0)
                        {
                            foreach (var item in list)
                            {
                                var filterItem = new FilterItem(item as IComparable);
                                filterList.Add(filterItem);
                            }
                        }
                        else
                        {
                            filterList.Add(new FilterItem(null));
                        }
                    }

                    // open the popup to display this list
                    popup.DataContext = filterList;
                    CollectionViewSource.GetDefaultView(filterList).Refresh();
                    popup.IsOpen = true;

                    // connect to the selection change event
                    var listView = (ListView) popup.Child;
                    listView.SelectionChanged += SelectionChangedHandler;
                }
            }
        }

        private static Type GetElementTypeOfEnumerable2(object o)
        {
            var enumerable = o as IEnumerable;
            // if it's not an enumerable why do you call this method all ?
            if (enumerable == null)
                return null;

            Type[] interfaces = enumerable.GetType().GetInterfaces();
            Type elementType = (from i in interfaces
                where i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>)
                select i.GetGenericArguments()[0]).FirstOrDefault();

            //peek at the first element in the list if you couldn't determine the element type
            if (elementType == null || elementType == typeof(object))
            {
                object firstElement = enumerable.Cast<object>().FirstOrDefault();
                if (firstElement != null)
                    elementType = firstElement.GetType();
            }

            return elementType;
        }

        #endregion Private Methods
    }
}