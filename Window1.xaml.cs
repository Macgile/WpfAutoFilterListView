﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;

// https://www.codeproject.com/Articles/27600/Developing-an-Autofilter-ListView


namespace WpfBindingFiltering
{
    /// <summary>
    /// Interaction logic for Window1.xaml
    /// </summary>
    public partial class Window1 : Window
    {
        private readonly IList<Employee> employees;


        public Window1()
        {
            InitializeComponent();

            employees = new List<Employee>();

            for (var i = 0; i < 10; i++)
            {
                employees.Add(CreateRandomEmployee());
            }

            DataContext = employees;
        }


        private Random rand = new Random();

        private static readonly string[] Surnames =
        {
            "Aaberg", "Adams", "Hanif", "Akers", "Harris", "Andersen",
            "Alberts", "Hamlin", "Hee", "Peno", "Pfeiffer", null
        };

        private static readonly string[] Forenames =
        {
            "Jesper", "Jonathan", "Kerim", "Kim", "Phyllis", "Elizabeth",
            "Amy", "Jay", "Gordon", "Lori", "Michael", null
        };

        private Employee CreateRandomEmployee()
        {
            return new Employee(Surnames[rand.Next(Surnames.Length)], Forenames[rand.Next(Forenames.Length)],
                rand.Next(100) * 100, new DateTime(1975 + rand.Next(22), rand.Next(12) + 1, rand.Next(28) + 1));
        }

        private void tabControl1_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            ICollectionView employeeView = CollectionViewSource.GetDefaultView(employees);
        }
    }
}